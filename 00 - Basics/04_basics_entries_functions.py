#Entries and Functions
import tkinter
from tkinter import END


#Functions
def make_label():
    '''Printa a label no output_frame'''
    text = tkinter.Label(output_frame, text=text_entry.get(), bg='#ff0000')
    text.pack()

    text_entry.delete(0,END)

def count_up(number):
    '''Aumenta o contador'''
    global value

    text = tkinter.Label(output_frame, text=number,bg='#FF0000')
    text.pack()

    value = number + 1
    
#Janela root
root = tkinter.Tk()
root.title('Básico sobre Funções e Entradas')
root.geometry('500x500')
root.resizable(0,0)
root.iconbitmap('Iconka-Business-Finance-Plane.ico')
root.config(bg='#EEEEE0')

#frames
input_frame = tkinter.Frame(root, bg='#0000ff',width=500,height=100)
output_frame = tkinter.Frame(root, bg='#ff0000',width=500,height=400)
input_frame.pack(padx=10,pady=10)
output_frame.pack(padx=10,pady=(0,10))

#Add inputs
text_entry = tkinter.Entry(input_frame, width=50)
text_entry.grid(row=0,column=0,padx=5,pady=5)
input_frame.grid_propagate(0)

print_button = tkinter.Button(input_frame, text="Print",command=make_label)
print_button.grid(row=0, column=1,padx=5,pady=5,ipadx=30)

#Manter o tamanho do output_frame
output_frame.pack_propagate(0)

#Passando parametros com lambda
value = 0
count_button = tkinter.Button(input_frame, text="Count!",command=lambda:count_up(value))
count_button.grid(row=1, column=0,columnspan=2,padx=5,pady=5,sticky="WE")

#main loop
root.mainloop()