import tkinter

#window
root = tkinter.Tk()
root.title('Label Basics')
root.iconbitmap('Iconka-Business-Finance-Plane.ico')
root.geometry('400x400')
root.resizable(0,0)
root.config(bg="blue")

#Criando widgets
name_label_1 = tkinter.Label(root, text='Hello, my name is Racicley.')
name_label_1.pack()

name_label_2 = tkinter.Label(root, text='Hello, my name is Pedro.',font=('Arial',18,'bold'))
name_label_2.pack()

name_label_3 = tkinter.Label(root)
name_label_3.config(text="Hello, my name is Racicley")
name_label_3.config(font=('Cambria',10))
name_label_3.config(bg='#FF0000')
name_label_3.pack(padx=10,pady=50)

name_label_4 = tkinter.Label(root, text='Hello, my name is Ana.',bg='#000000',fg="green")
name_label_4.pack(pady=(0,10), ipadx=50, ipady='10', anchor='w')


name_label_5 = tkinter.Label(root, text='Hello, my name is Pedro.',bg='#FFFFFF',fg="#123456")
name_label_5.pack(fill='both', expand=True, padx=10, pady=10)

#main loop
root.mainloop()