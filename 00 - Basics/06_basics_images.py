#Images
import tkinter
from PIL import ImageTk, Image

#function
def make_image():
    global ant_lion
    #Usando PIL para jpg
    ant_lion = ImageTk.PhotoImage(Image.open('formiga_leao.jpg'))
    ant_lion_label = tkinter.Label(root, image=ant_lion)
    ant_lion_label.pack()

#window
root = tkinter.Tk()
root.geometry('700x700')
root.title('Images')
root.iconbitmap('Iconka-Business-Finance-Plane.ico')


#Basics funciona para png
my_image = tkinter.PhotoImage(file='exploration.png')
my_img_label = tkinter.Label(root, image=my_image)
#my_img_label.pack()

my_button = tkinter.Button(root, image=my_image)
#my_button.pack()

#Não funciona para jpg
#ant_lion = tkinter.PhotoImage(file='formiga_leao.jpg')
#ant_lion_label = tkinter.Label(root, image=ant_lion)
#ant_lion_label.pack()

#Usando PIL para jpg
#ant_lion = ImageTk.PhotoImage(Image.open('formiga_leao.jpg'))
#ant_lion_label = tkinter.Label(root, image=ant_lion)
#ant_lion_label.pack()

make_image()

#main loop
root.mainloop()