#Window basics
import tkinter

#Janela Root
root = tkinter.Tk()

#Trocando o título da janela root       
root.title('Janela ROOT')

#Icone da janela
root.iconbitmap('Iconka-Business-Finance-Plane.ico')

#width x height
root.geometry('250x700')

#Alterando a possibilidade de ajustar a janela, x e y
root.resizable(0,0)

#Config de uma widgets, no caso o janela root
root.config(bg="blue")

#Segunda janela
top = tkinter.Toplevel()

#titulo
top.title('Segunda Janela')

#Cor
top.config(bg="yellow")

#widthxheight+x+y, nesse caso x e y, são descolacamentos positivos
top.geometry('250x250+0+0')

# Rodando o main loop da janela root
root.mainloop()