#Frames
import tkinter
from tkinter import BOTH

#Janela Root
root = tkinter.Tk()
root.title('Frames Basics')
root.iconbitmap('Iconka-Business-Finance-Plane.ico')
root.geometry('500x500')
root.resizable(0,0)
root.config(bg='#e1FeF2')

#Examplo de como se usa frames
#name_label = tkinter.Label(root, text='Entre com seu nome')
#name_label.pack()

#name_button = tkinter.Button(root, text='Enviar')
#name_button.grid(row=0,column=1)

#Frames
pack_frame = tkinter.Frame(root, bg="red")
grid_frame_1 = tkinter.Frame(root, bg="blue")
grid_frame_2 = tkinter.LabelFrame(root, text='Grid system rules!',borderwidth=5)

#pack frames na root
pack_frame.pack(fill=BOTH,expand=True)
grid_frame_1.pack(fill='x',expand=True)
grid_frame_2.pack(fill=BOTH,expand=True,padx=10,pady=10)

#pack frame
tkinter.Label(pack_frame, text='text').pack()
tkinter.Label(pack_frame, text='text').pack()
tkinter.Label(pack_frame, text='text').pack()

#Grid 1 Layout
tkinter.Label(grid_frame_1, text='text').grid(row=0,column=0)
tkinter.Label(grid_frame_1, text='text').grid(row=1,column=1)
tkinter.Label(grid_frame_1, text='text').grid(row=2,column=2)
#tkinter.Label(grid_frame_1, text="ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ").grid(row=3,column=0)

#Grrid 2 Layout
tkinter.Label(grid_frame_2, text="ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ").grid(row=0,column=0)

#main loop
root.mainloop()